<?php

Route::get('/','LoginController@index');

Route::get('/inicio','InicioController@index')->middleware('auth');

Route::get('/edificios','BuildingController@index')->middleware('auth');
Route::get('/edificio/{id}', ['uses' => 'BuildingController@edit'])->middleware('auth')->name('buildings.edit');
Route::post('/editBuilding', 'BuildingController@editBuilding')->middleware('auth');
Route::post('/addBuilding', 'BuildingController@addBuilding')->middleware('auth');
Route::get('/edificio-nuevo', ['uses' => 'BuildingController@add'])->middleware('auth')->name('buildings.add');
Route::delete('/deleteBuilding/{id}', 'BuildingController@deleteBuilding')->middleware('auth')->name('buildings.delete');

Route::get('/auditorios','AuditoriumController@index')->middleware('auth');
Route::get('/auditorio/{id}', ['uses' => 'AuditoriumController@edit'])->middleware('auth')->name('auditoria.edit');
Route::post('/editAuditorium', 'AuditoriumController@editAuditorium')->middleware('auth');
Route::post('/addAuditorium', 'AuditoriumController@addAuditorium')->middleware('auth');
Route::get('/auditorio-nuevo', ['uses' => 'AuditoriumController@add'])->middleware('auth')->name('auditoria.add');
Route::delete('/deleteAuditorium/{id}', 'AuditoriumController@deleteAuditorium')->middleware('auth')->name('auditoria.delete');

Route::get('/cafeterias','CafeteriaController@index')->middleware('auth');
Route::get('/cafeteria/{id}', ['uses' => 'CafeteriaController@edit'])->middleware('auth')->name('cafeterias.edit');
Route::post('/editCafeteria', 'CafeteriaController@editCafeteria')->middleware('auth');
Route::post('/addCafeteria', 'CafeteriaController@addCafeteria')->middleware('auth');
Route::get('/cafeteria-nueva', ['uses' => 'CafeteriaController@add'])->middleware('auth')->name('cafeterias.add');
Route::delete('/deleteCafeteria/{id}', 'CafeteriaController@deleteCafeteria')->middleware('auth')->name('cafeterias.delete');

Route::get('/papelerias','StationeryController@index')->middleware('auth');
Route::get('/papeleria/{id}', ['uses' => 'StationeryController@edit'])->middleware('auth')->name('stationeries.edit');
Route::post('/editStationery', 'StationeryController@editStationery')->middleware('auth');
Route::post('/addStationery', 'StationeryController@addStationery')->middleware('auth');
Route::get('/papeleria-nueva', ['uses' => 'StationeryController@add'])->middleware('auth')->name('stationeries.add');
Route::delete('/deleteStationery/{id}','StationeryController@deleteStationery')->middleware('auth')->name('stationeries.delete');

Route::get('/servicios','ServiceController@index')->middleware('auth');
Route::get('/servicio/{id}', ['uses' => 'ServiceController@edit'])->middleware('auth')->name('services.edit');
Route::post('/editService', 'ServiceController@editService')->middleware('auth');
Route::post('/addService', 'ServiceController@addService')->middleware('auth');
Route::get('/servicio-nuevo', ['uses' => 'ServiceController@add'])->middleware('auth')->name('services.add');
Route::delete('/deleteService/{id}', 'ServiceController@deleteService')->middleware('auth')->name('services.delete');

Route::get('/configuracion','ConfigController@index')->middleware('auth');

Route::get('/mensajes','MessagesController@index')->middleware('auth');
Route::delete('/deleteMessage/{id}', 'MessagesController@deleteMessage')->middleware('auth')->name('messages.delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
Route::post('/changeEmail','HomeController@changeEmail')->name('changeEmail');
Route::get('backups', 'BackupController@index')->middleware('auth');
Route::get('create', 'BackupController@respaldoPhp')->middleware('auth');
Route::get('backup/download/{file_name}', 'BackupController@download')->middleware('auth');
Route::get('backup/restore/{file_name}', 'BackupController@restore')->middleware('auth');
Route::get('backup/delete/{file_name}', 'BackupController@delete')->middleware('auth');
