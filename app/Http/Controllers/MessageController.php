<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Message;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function index()
    {
        $messages = DB::table('messages')
        ->orderBy('fecha', 'desc')
        ->get();
        return view('messages.index', compact('messages'));
    }
}
