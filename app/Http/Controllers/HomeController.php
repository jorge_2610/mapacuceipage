<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            return redirect()->back()->with("error","Error, La contraseña que introdujiste no coincide con la contraseña actual.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            return redirect()->back()->with("error","Error, La nueva contraseña debe ser diferente que tu contraseña actual.");
        }

        if(strcmp($request->get('new-password_confirmation'), $request->get('new-password')) != 0){
            return redirect()->back()->with("error","Error, Las contraseñas nueva no coincide con la confirmación de contraseña.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","La contraseña se cambió correctamente");
    }


    public function changeEmail(Request $request){

        if(strcmp($request->get('email'), Auth::user()->email) != 0){
            //There are  no users with thah email
            alert()->error('Ese correo no coincide con los registros.','Error.')->autoclose(3000);
            return redirect()->back();
        }

        if(strcmp(Auth::user()->email, $request->get('new-email')) == 0){
            //Current email and the one provided are not the same
            alert()->error('El nuevo correo debe ser diferente que tu correo actual.','Error.')->autoclose(3000);
            return redirect()->back();
        }

        $user = Auth::user();
        $user->email = $request->get('new-email');
        $user->save();

        alert()->success('Se actualizó correctamente el correo','Éxito.')->autoclose(3000);
        return redirect()->back();
    }
}
