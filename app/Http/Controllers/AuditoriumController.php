<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Auditorium;
use App\Http\Controllers\Controller;
use Storage;

class AuditoriumController extends Controller
{
    public function index(){
    	$auditoriums = Auditorium::all();
		  return view('auditoria.index', compact('auditoriums'));
    }

    public function add() {
        $config = array();
        $config['center'] = '20.657395, -103.324954';
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng(20.657395, -103.324954)
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('auditoria.add', compact('map','config','marker'));
    }

    public function addAuditorium (Request $request) {
        $auditorium = new Auditorium();
        $name = DB::table('auditoria')->where('nombre', '=', $request->input("nombre"))->get();
        if(count($name)!=0){
            alert()->error('Ya existe auditorio con ese nombre','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $auditorium->nombre = $request->input("nombre");
        $auditorium->descripcion = $request->input("descripcion");
        $auditorium->latitud = $request->input("latitudes");
        $auditorium->longitud = $request->input("longitudes");
        $auditorium->save();
        $filename = str_replace(' ', '', $auditorium->nombre);
        $filename = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $filename );
        $filename = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $filename );
        $filename = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $filename );
        $filename = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $filename );
        $filename = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $filename );
        $filename = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $filename );
        if ($request->hasFile('files')) {
            foreach($request->file('files') as $file){
                $filenamewithextension = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $filenametostore = $filename.'.'.$extension;
                $img_f = $file->getPathName();
                $imagen = $filename.'.'.$extension;
                Storage::disk('public')->put($imagen, fopen($file, 'r+'));
            }
        }
        $output = 
        '<html lang="es">
            <head>
                <title></title>
                <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
                <style type="text/css">body{
                            font:14px helvetica;
                            background-repeat: no-repeat; 
                            background-position: center;
                            background-attachment: fixed;       
                            webkit-background-size: cover;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;
                        }
                        h1{
                            font-weight: bold;
                            font-size: 24px;
                            right: 20px;
                            left: 9px;
                            color: #197EEA;
                            text-shadow: #fff 0 1px 0;
                            padding: 1px 0 3px 3px;
                        }
                        h2{
                            font-weight: bold;
                            font-size: 17px;
                            right: 20px;
                            left: 9px;
                            color: #197EEA;
                            text-shadow: #fff 0 1px 0;
                            padding: 1px 0 3px 8px;
                        }
                        .contenido{
                            font-weight: bold;
                            font-size: 14px;
                            right: 20px;
                            left: 9px;
                            color: #282828;
                            text-shadow: #fff 0 1px 0;
                            padding: 1px 0 3px 5px;
                        }
                        .photoContainer {
                            overflow: auto;
                            height: auto;
                            width: 100%;
                            font-size: 20px;
                        }
                        .title {
                            text-align: center;
                        }
                </style>
            </head>
            <body>
                <p>
                    <img src="logocucei.png" width="100%" />
                </p>
                <hr/>
                <div style="text-align: center;">
                    <img src="cinema.png" style="height: 50px;" />
                    <h1 class="title">'.$auditorium->nombre.'</h1>
                </div>
                
                <div class="photoContainer">
                    <img src="'.$filename.'.jpg" style="height: 300px;" />
                </div>
                <h2>Descripción:</h2>
                
                <div class="contenido">'.$auditorium->descripcion.'</div>
            </body>
        </html>';
        $filename = strtolower($filename);
        $filename .= ".html";
        Storage::disk('public')->put($filename, $output);
        alert()->success('Auditorio Agregado correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }

    public function edit($id)
    {
        $auditorium = Auditorium::find($id);

        $config = array();
        $config['center'] = $auditorium->latitud.','.$auditorium->longitud;
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng('.$auditorium->latitud.', '.$auditorium->longitud.')
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('auditoria.edit', compact('map','auditorium','config','marker'));
    }

    public function deleteAuditorium($id){
        $auditorium = Auditorium::find($id);
        $auditoriumName = $auditorium->nombre;
        $auditoriumName = str_replace(' ', '', $auditorium->nombre);
        $auditoriumName = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $auditoriumName );
        $auditoriumName = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $auditoriumName );
        $auditoriumName = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $auditoriumName );
        $auditoriumName = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $auditoriumName );
        $auditoriumName = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $auditoriumName );
        $auditoriumName = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $auditoriumName );
        Storage::disk('public')->delete($auditoriumName.'.jpg');
        Storage::disk('public')->delete($auditoriumName.'.html');
        DB::table("auditoria")->delete($id);
        return redirect()->back();
    }


    public function editAuditorium(Request $request)
    {
        $id = $request->input("id");
        $auditorium = Auditorium::find($id);
        $name = DB::table('auditoria')->where('nombre', '=', $request->input("nombre"))->get();
        if(count($name) != 0 && $request->input("nombre") != $auditorium->nombre){
            alert()->error('Ya existe auditorio con ese nombre','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $lastNameFile = $auditorium->nombre;
        $lastNameFile = str_replace(' ', '', $lastNameFile);
        $lastNameFile = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $lastNameFile);
        $lastNameFile = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $lastNameFile);
        $auditorium->nombre = $request->input("nombre");
        $auditorium->descripcion = $request->input("descripcion");
        $auditorium->latitud = $request->input("latitudes");
        $auditorium->longitud = $request->input("longitudes");
        $auditorium->save();
        $filename = str_replace(' ', '', $auditorium->nombre);
        $filename = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $filename );
        $filename = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $filename );
        $filename = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $filename );
        $filename = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $filename );
        $filename = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $filename );
        $filename = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $filename );
        alert()->success('Auditorio Editado correctamente','Éxito.')->autoclose(3000);
        $output = 
        '<html lang="es">
        <head>
            <title>'.$auditorium->nombre.'</title>
            <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
            <style type="text/css">
                    body{
                        font:14px helvetica;
                        background-repeat: no-repeat; 
                        background-position: center;
                        background-attachment: fixed;       
                        webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }
                    h1{
                        font-weight: bold;
                        font-size: 24px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 3px;
                    }
                    h2{
                        font-weight: bold;
                        font-size: 17px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .contenido{
                        font-weight: bold;
                        font-size: 14px;
                        right: 20px;
                        left: 9px;
                        color: #282828;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 5px;
                    }
                    .photoContainer {
                        overflow: auto;
                        height: auto;
                        width: 100%;
                        font-size: 20px;
                    }
                    .title {
                        text-align: center;
                    }
            </style>
        </head>
        <body>
        <p><img src="logocucei.png" width="100%" /></p>
        
        <hr />
        <div style="text-align: center;"><img src="cinema.png" style="height: 50px;" />
        <h1 class="title">'.$auditorium->nombre.'</h1>
        </div>
        
        <div class="photoContainer"><img src="'.$filename.'.jpg" style="height: 300px;" /></div>
        
        <h2>Descripci&oacute;n:</h2>
        
        <div class="contenido">'.$auditorium->descripcion.'</div>
        </body>
        </html>';
        $disk = Storage::disk(config('../storage/app'));
        if($filename != $lastNameFile){
            Storage::disk('public')->put($filename.'.jpg', Storage::disk('public')->get($lastNameFile.'.jpg'));
            Storage::disk('public')->delete($lastNameFile.'.jpg');
            Storage::disk('public')->delete($lastNameFile.'.html');
        }
        $filename = strtolower($filename);
        $filename .= ".html";
        if ($disk->exists(config('../storage/app/public') . '/' . $filename)) {
            $disk->delete(config('../storage/app/public') . '/' . $filename);
        }
        Storage::disk('public')->put($filename, $output);
        alert()->success('Auditorio Editado correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }
}
