<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;

class MapController extends Controller
{
    public function edit($id)
    {
        
        $building = Building::find($id);

        $config = array();
        $config['center'] = $building->latitud.','.$building->longitud;
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng('.$building->latitud.', '.$building->longitud.')
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('gmaps', compact('map','building','config','marker'));
    }
}
