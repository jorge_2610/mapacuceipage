<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Stationery;
use App\Http\Controllers\Controller;
use Storage;

class StationeryController extends Controller
{
    public function index(){
    	$stationeries = Stationery::all();
		return view('stationeries/index', compact('stationeries'));
    }

    public function add() {
        $config = array();
        $config['center'] = '20.657395, -103.324954';
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng(20.657395, -103.324954)
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('stationeries.add', compact('map','config','marker'));
    }

    public function edit($id)
    {
        
        $stationery = Stationery::find($id);

        $config = array();
        $config['center'] = $stationery->latitud.','.$stationery->longitud;
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng('.$stationery->latitud.', '.$stationery->longitud.')
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('stationeries.edit', compact('map','stationery','config','marker'));
    }

    public function addStationery (Request $request) {
        $stationery = new Stationery();
        $name = DB::table('stationeries')->where('nombre', '=', $request->input("nombre"))->get();
        if(count($name)!=0){
            alert()->error('Ya existe papelería con ese nombre','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $horaApertura = strtotime($request->input("hora_apertura"));
        $horaCierre = strtotime($request->input("hora_cierre"));
        if( $horaApertura > $horaCierre ) {
            alert()->error('La hora de cierre debe ser mayor a la hora de apertura.Intente con valores válidos','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $stationery->nombre = $request->input("nombre");
        $stationery->descripcion = $request->input("descripcion");
        $stationery->hora_apertura = $request->input("hora_apertura");
        $stationery->hora_cierre = $request->input("hora_cierre");
        $stationery->latitud = $request->input("latitudes");
        $stationery->longitud = $request->input("longitudes");
        $stationery->save();
        $filename = str_replace(' ', '', $stationery->nombre);
        $filename = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $filename );
        $filename = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $filename );
        $filename = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $filename );
        $filename = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $filename );
        $filename = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $filename );
        $filename = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $filename );
        if ($request->hasFile('files')) {
            foreach($request->file('files') as $file){
                $filenamewithextension = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $filenametostore = $filename.'.'.$extension;
                $img_f = $file->getPathName();
                $imagen = $filename.'.'.$extension;
                Storage::disk('public')->put($imagen, fopen($file, 'r+'));
            }
        }
        $output = '<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
                <title></title> 
                <style type="text/css">
                    body{
                        font:14px helvetica;
                        background-repeat: no-repeat; 
                        background-position: center;
                        background-attachment: fixed;       
                        webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }
                    h1{
                        font-weight: bold;
                        font-size: 24px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 3px;
                    }
                    h2{
                        font-weight: bold;
                        font-size: 17px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .contenido{
                        font-weight: bold;
                        font-size: 14px;
                        right: 20px;
                        left: 9px;
                        color: #282828;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 5px;
                    }
                    .photoContainer {
                        overflow: auto;
                        height: auto;
                        width: 100%;
                        font-size: 20px;
                    }
                    .title {
                        text-align: center;
                    }
                </style>
            </head>
            <body>
                <p>
                    <img src="logocucei.png" width="100%" />
                </p>
                <hr/>
                <div style="text-align: center;"><img src="fast-food.png" style="height: 50px;" />
                    <h1 class="title">'.$stationery->nombre.'</h1>
                </div>
                <div class="photoContainer">
                    <img src="'.$filename.'.jpg" style="height: 300px;" />
                </div>
                <h2>Descripci&oacute;n:</h2>
            <div class="contenido">
                '.$stationery->descripcion.'
            </div>

            <div class="contenido">
            <h2>Horarios:</h2>

            <ul>
                <li>Lunes a Viernes De '.$stationery->hora_apertura.' a '.$stationery->hora_cierre.'</li>
            </ul>
            </div>
            </body>
        </html>';
        $filename = strtolower($filename);
        $filename .= ".html";
        Storage::disk('public')->put($filename, $output);
        alert()->success('Papelería Agregado correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }

    public function deleteStationery($id){
        $stationery = Stationery::find($id);
        $stationeryName = $stationery->nombre;
        $stationeryName = str_replace(' ', '', $stationery->nombre);
        $stationeryName = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $stationeryName );
        $stationeryName = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $stationeryName );
        $stationeryName = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $stationeryName );
        $stationeryName = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $stationeryName );
        $stationeryName = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $stationeryName );
        $stationeryName = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $stationeryName );
        Storage::disk('public')->delete($stationeryName.'.jpg');
        Storage::disk('public')->delete($stationeryName.'.html');
        DB::table("stationeries")->delete($id);
        return back();
    }

    public function editStationery(Request $request)
    {
        $id = $request->input("id");
        $stationery = Stationery::find($id);
        $name = DB::table('stationeries')->where('nombre', '=', $request->input("nombre"))->get();
        if(count($name) != 0 && $request->input("nombre") != $stationery->nombre){
            alert()->error('Ya existe papelería con ese nombre','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $horaApertura = strtotime($request->input("hora_apertura"));
        $horaCierre = strtotime($request->input("hora_cierre"));
        if( $horaApertura > $horaCierre ) {
            alert()->error('La hora de cierre debe ser mayor a la hora de apertura.Intente con valores válidos','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $lastNameFile = $stationery->nombre;
        $lastNameFile = str_replace(' ', '', $lastNameFile);
        $lastNameFile = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $lastNameFile);
        $lastNameFile = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $lastNameFile);
        $stationery->nombre = $request->input("nombre");
        $stationery->descripcion = $request->input("descripcion");
        $stationery->hora_apertura = $request->input("hora_apertura");
        $stationery->hora_cierre = $request->input("hora_cierre");
        $stationery->latitud = $request->input("latitudes");
        $stationery->longitud = $request->input("longitudes");
        $stationery->save();
        $filename = str_replace(' ', '', $stationery->nombre);
        $filename = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $filename
        );
        $filename = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $filename );
        $filename = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $filename );
        $filename = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $filename );
        $filename = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $filename );
        $filename = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $filename
        );
        $output = 
        '<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
                <title></title>
                <style type="text/css">
                    body{
                        font:14px helvetica;
                        background-repeat: no-repeat; 
                        background-position: center;
                        background-attachment: fixed;       
                        webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }
                    h1{
                        font-weight: bold;
                        font-size: 24px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 3px;
                    }
                    h2{
                        font-weight: bold;
                        font-size: 17px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .contenido{
                        font-weight: bold;
                        font-size: 14px;
                        right: 20px;
                        left: 9px;
                        color: #282828;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 5px;
                    }
                    .photoContainer {
                        overflow: auto;
                        height: auto;
                        width: 100%;
                        font-size: 20px;
                    }
                    .title {
                        text-align: center;
                    }
                </style>
            </head>
            <body>
                <p>
                    <img src="logo.png" width="100%" />
                </p>
                <hr/>
                <div style="text-align: center;">
                    <img src="brujula.png" style="height: 50px;" />
                    <h1 class="title">'.$stationery->nombre.'</h1>
                </div>
                <div class="photoContainer">
                    <img src="'.$filename.'.jpg" style="height: 300px;" />
                </div>

                <h2>Descripci&oacute;n:</h2>

                <div class="contenido">
                    '.$stationery->descripcion.'
                </div>

                <div class="contenido">
                    <h2>Horarios:</h2>
                    <ul>
                        <li>Lunes a Viernes de '.$stationery->hora_apertura.' a '.$stationery->hora_cierre.'</li>
                    </ul>
                </div>
            </body>
        </html>';
        $disk = Storage::disk(config('../storage/app'));
        if($filename != $lastNameFile){
            Storage::disk('public')->put($filename.'.jpg', Storage::disk('public')->get($lastNameFile.'.jpg'));
            Storage::disk('public')->delete($lastNameFile.'.jpg');
            Storage::disk('public')->delete($lastNameFile.'.html');
        }
        $filename = strtolower($filename);
        $filename .= ".html";
        if ($disk->exists(config('../storage/app/public') . '/' . $filename)) {
            $disk->delete(config('../storage/app/public') . '/' . $filename);
        }
        Storage::disk('public')->put($filename, $output);
        alert()->success('Cafeteria Editada correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }
}