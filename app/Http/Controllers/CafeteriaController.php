<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cafeteria;
use App\Http\Controllers\Controller;
use Storage;

class CafeteriaController extends Controller
{
    public function index(){
    	$cafeterias = Cafeteria::all();
		  return view('cafeterias/index', compact('cafeterias'));
    }

    public function add() {
        $config = array();
        $config['center'] = '20.657395, -103.324954';
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng(20.657395, -103.324954)
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('cafeterias.add', compact('map','config','marker'));
    }

    public function addCafeteria (Request $request) {
        $cafeteria = new Cafeteria();
        $name = DB::table('cafeterias')->where('nombre', '=', $request->input("nombre"))->get();
        if(count($name)!=0){
            alert()->error('Ya existe cafetería con ese nombre','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $horaApertura = strtotime($request->input("hora_apertura"));
        $horaCierre = strtotime($request->input("hora_cierre"));
        if( $horaApertura > $horaCierre ) {
            alert()->error('La hora de cierre debe ser mayor a la hora de apertura.Intente con valores válidos','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $cafeteria->nombre = $request->input("nombre");
        $cafeteria->descripcion = $request->input("descripcion");
        $cafeteria->menu = $request->input("menu");
        $cafeteria->hora_apertura = $request->input("hora_apertura");
        $cafeteria->hora_cierre = $request->input("hora_cierre");
        $cafeteria->latitud = $request->input("latitudes");
        $cafeteria->longitud = $request->input("longitudes");
        $cafeteria->save();
        $filename = str_replace(' ', '', $cafeteria->nombre);
        $filename = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $filename );
        $filename = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $filename );
        $filename = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $filename );
        $filename = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $filename );
        $filename = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $filename );
        $filename = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $filename );
        if ($request->hasFile('files')) {
            foreach($request->file('files') as $file){
                $filenamewithextension = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $filenametostore = $filename.'.'.$extension;
                $img_f = $file->getPathName();
                $imagen = $filename.'.'.$extension;
                Storage::disk('public')->put($imagen, fopen($file, 'r+'));
            }
        }
        $cafeteria->menu = str_replace("\n", "</li><li>", $cafeteria->menu);
        $output = '<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
                <title></title> 
                <style type="text/css">
                    body{
                        font:14px helvetica;
                        background-repeat: no-repeat; 
                        background-position: center;
                        background-attachment: fixed;       
                        webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }
                    h1{
                        font-weight: bold;
                        font-size: 24px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 3px;
                    }
                    h2{
                        font-weight: bold;
                        font-size: 17px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .contenido{
                        font-weight: bold;
                        font-size: 14px;
                        right: 20px;
                        left: 9px;
                        color: #282828;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 5px;
                    }
                    .photoContainer {
                        overflow: auto;
                        height: auto;
                        width: 100%;
                        font-size: 20px;
                    }
                    .title {
                        text-align: center;
                    }
                </style>
            </head>
            <body>
                <p>
                    <img src="logocucei.png" width="100%" />
                </p>
                <hr/>
                <div style="text-align: center;"><img src="fast-food.png" style="height: 50px;" />
                    <h1 class="title">'.$cafeteria->nombre.'</h1>
                </div>
                <div class="photoContainer">
                    <img src="'.$filename.'.jpg" style="height: 300px;" />
                </div>
                <h2>Descripción:</h2>
                <div class="contenido">
                    '.$cafeteria->descripcion.'
                </div>
                <h2>Menú:</h2>
                <div class="contenido">
                    <ul>
                        <li>'.$cafeteria->menu.'</li>
                    </ul>
                </div>

                <div class="contenido">
                <h2>Horarios:</h2>

                <ul>
                    <li>Lunes a Viernes De '.$cafeteria->hora_apertura.' a '.$cafeteria->hora_cierre.'</li>
                </ul>
            </div>
            </body>
        </html>';
        $filename = strtolower($filename);
        $filename .= ".html";
        Storage::disk('public')->put($filename, $output);
        alert()->success('Cafeteria Agregada correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }

    public function edit($id)
    {
        $cafeteria = Cafeteria::find($id);

        $config = array();
        $config['center'] = $cafeteria->latitud.','.$cafeteria->longitud;
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng('.$cafeteria->latitud.', '.$cafeteria->longitud.')
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('cafeterias.edit', compact('map','cafeteria','config','marker'));
    }

    public function deleteCafeteria($id){
        $cafeteria = Cafeteria::find($id);
        $cafeteriaName = $cafeteria->nombre;
        $cafeteriaName = str_replace(' ', '', $cafeteria->nombre);
        $cafeteriaName = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cafeteriaName );
        $cafeteriaName = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cafeteriaName );
        $cafeteriaName = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cafeteriaName );
        $cafeteriaName = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cafeteriaName );
        $cafeteriaName = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cafeteriaName );
        $cafeteriaName = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $cafeteriaName );
        Storage::disk('public')->delete($cafeteriaName.'.jpg');
        Storage::disk('public')->delete($cafeteriaName.'.html');
        DB::table("cafeterias")->delete($id);
        return redirect()->back();
    }

    public function editCafeteria(Request $request)
    {
        $id = $request->input("id");
        $cafeteria = Cafeteria::find($id);
        $name = DB::table('cafeterias')->where('nombre', '=', $request->input("nombre"))->get();
        if(count($name) != 0 && $request->input("nombre") != $cafeteria->nombre){
            alert()->error('Ya existe auditorio con ese nombre','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $horaApertura = strtotime($request->input("hora_apertura"));
        $horaCierre = strtotime($request->input("hora_cierre"));
        if( $horaApertura > $horaCierre ) {
            alert()->error('La hora de cierre debe ser mayor a la hora de apertura.Intente con valores válidos','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $lastNameFile = $cafeteria->nombre;
        $lastNameFile = str_replace(' ', '', $lastNameFile);
        $lastNameFile = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $lastNameFile);
        $lastNameFile = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $lastNameFile);
        $cafeteria->nombre = $request->input("nombre");
        $cafeteria->descripcion = $request->input("descripcion");
        $cafeteria->menu = $request->input("menu");
        $cafeteria->hora_apertura = $request->input("hora_apertura");
        $cafeteria->hora_cierre = $request->input("hora_cierre");
        $cafeteria->latitud = $request->input("latitudes");
        $cafeteria->longitud = $request->input("longitudes");
        $cafeteria->save();
        $filename = str_replace(' ', '', $cafeteria->nombre);
        $filename = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $filename
        );
        $filename = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $filename );
        $filename = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $filename );
        $filename = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $filename );
        $filename = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $filename );
        $filename = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $filename
        );
        $cafeteria->menu = str_replace("\n", "</li><li>", $cafeteria->menu);
        $output = 
        '<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
                <title></title> 
                <style type="text/css">
                    body{
                        font:14px helvetica;
                        background-repeat: no-repeat; 
                        background-position: center;
                        background-attachment: fixed;       
                        webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }
                    h1{
                        font-weight: bold;
                        font-size: 24px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 3px;
                    }
                    h2{
                        font-weight: bold;
                        font-size: 17px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .contenido{
                        font-weight: bold;
                        font-size: 14px;
                        right: 20px;
                        left: 9px;
                        color: #282828;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 5px;
                    }
                    .photoContainer {
                        overflow: auto;
                        height: auto;
                        width: 100%;
                        font-size: 20px;
                    }
                    .title {
                        text-align: center;
                    }
                </style>
            </head>
            <body>
                <p>
                    <img src="logocucei.png" width="100%" />
                </p>
                <hr/>
                <div style="text-align: center;"><img src="fast-food.png" style="height: 50px;" />
                    <h1 class="title">'.$cafeteria->nombre.'</h1>
                </div>
                <div class="photoContainer">
                    <img src="'.$filename.'.jpg" style="height: 300px;" />
                </div>
                <h2>Descripci&oacute;n:</h2>
                <div class="contenido">
                    '.$cafeteria->descripcion.'
                </div>
                <h2>Menú</h2>
                <div class="contenido">
                    <ul>
                        <li>'.$cafeteria->menu.'</li>
                    </ul>
                </div>

                <div class="contenido">
                    <h2>Horarios:</h2>

                    <ul>
                        <li>Lunes a Viernes De '.$cafeteria->hora_apertura.' a '.$cafeteria->hora_cierre.'</li>
                    </ul>
                </div>
            </body>
        </html>';
        $disk = Storage::disk(config('../storage/app'));
        if($filename != $lastNameFile){
            Storage::disk('public')->put($filename.'.jpg', Storage::disk('public')->get($lastNameFile.'.jpg'));
            Storage::disk('public')->delete($lastNameFile.'.jpg');
            Storage::disk('public')->delete($lastNameFile.'.html');
        }
        $filename = strtolower($filename);
        $filename .= ".html";
        if ($disk->exists(config('../storage/app/public') . '/' . $filename)) {
            $disk->delete(config('../storage/app/public') . '/' . $filename);
        }
        Storage::disk('public')->put($filename, $output);
        alert()->success('Cafeteria Editada correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }
}
