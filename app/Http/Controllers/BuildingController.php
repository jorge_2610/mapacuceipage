<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Building;
use App\Http\Controllers\Controller;
use Storage;

class BuildingController extends Controller
{
    public function index(){
    	$buildings = Building::all();
		return view('buildings.index', compact('buildings'));
    }

    public function edit($id)
    {   
        $building = Building::find($id);

        $config = array();
        $config['center'] = $building->latitud.','.$building->longitud;
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng('.$building->latitud.', '.$building->longitud.')
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('buildings.edit', compact('map','building','config','marker'));
    }

    public function deleteBuilding($id){
        $building = Building::find($id);
        $buildingName = $building->nombre;
        $buildingName = str_replace(' ', '', $building->nombre);
        $buildingName = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $buildingName
        );
        $buildingName = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $buildingName );
        $buildingName = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $buildingName );
        $buildingName = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $buildingName );
        $buildingName = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $buildingName );
        $buildingName = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $buildingName
        );
        Storage::disk('public')->delete($buildingName.'.jpg');
        Storage::disk('public')->delete($buildingName.'.html');
        DB::table("buildings")->delete($id);
        return redirect()->back();
    }


    public function addBuilding (Request $request) {
        $building = new Building();
        $name = DB::table('buildings')->where('nombre', '=', $request->input("nombre"))->get();
        if(count($name)!=0){
            alert()->error('Ya existe edificio con ese nombre','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $building->nombre = $request->input("nombre");
        $building->descripcion = $request->input("descripcion");
        $building->dependencias = $request->input("dependencias");
        $building->latitud = $request->input("latitudes");
        $building->longitud = $request->input("longitudes");
        $building->save();
        $filename = str_replace(' ', '', $building->nombre);
        $filename = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $filename
        );
        $filename = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $filename );
        $filename = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $filename );
        $filename = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $filename );
        $filename = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $filename );
        $filename = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $filename
        );
        if ($request->hasFile('files')) {
            foreach($request->file('files') as $file){
                $filenamewithextension = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $filenametostore = $filename.'.'.$extension;
                $img_f = $file->getPathName();
                $imagen = $filename.'.'.$extension;
                Storage::disk('public')->put($imagen, fopen($file, 'r+'));
            }
        }
        $building->dependencias = str_replace("\n", "</li><li>", $building->dependencias);
        $output = 
        '<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
                <style>
                    body{
                        font:14px helvetica;
                        background-repeat: no-repeat; 
                        background-position: center;
                        background-attachment: fixed;       
                        webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }
                    h1{
                        font-weight: bold;
                        font-size: 20px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .contenido{
                        font-weight: bold;
                        font-size: 14px;
                        right: 20px;
                        left: 9px;
                        color: #282828;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .photoContainer {
                        overflow: auto;
                        height: auto;
                        width: 100%;
                        font-size: 20px;
                    }
                </style>
            </head>
            <body>
                <img width="100%" src="logo.png">
                <hr>
                <div class="photoContainer">
                    <img src="'.$filename.'.jpg" style="height: 250px;">
                </div>
                <h1>Descripci&oacute;n:</h1>
                <div class="contenido">
                    <!--Poner la descripcion del modulo aqui-->
                    '.$building->descripcion.'
                </div>
                <h1>Dependencias:</h1>
                <div class="contenido">
                    <!--Poner las oficinas y demas que encontramos en cada modulo aqui-->
                    <ul>
                        <li>'.$building->dependencias.'</li>
                    </ul>
                </div>
            </body>
        </html>';
        $filename = strtolower($filename);
        $filename .= ".html";
        Storage::disk('public')->put($filename, $output);
        alert()->success('Edificio Agregado correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }


    public function editBuilding(Request $request)
    {
        $id = $request->input("id");
        $building = Building::find($id);
        $name = DB::table('buildings')->where('nombre', '=', $request->input("nombre"))->get();
        if(count($name)!=0 && $request->input("nombre")!=$building->nombre){
            alert()->error('Ya existe edificio con ese nombre','Error.')->autoclose(3000);
            return redirect()->back();
        }
        $lastNameFile = $building->nombre;
        $lastNameFile = str_replace(' ', '', $lastNameFile);
        $lastNameFile = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $lastNameFile);
        $lastNameFile = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $lastNameFile );
        $lastNameFile = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $lastNameFile);
        $building->nombre = $request->input("nombre");
        $building->descripcion = $request->input("descripcion");
        $building->dependencias = $request->input("dependencias");
        $building->latitud = $request->input("latitudes");
        $building->longitud = $request->input("longitudes");
        $building->save();
        $filename = str_replace(' ', '', $building->nombre);
        $filename = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $filename);
        $filename = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $filename );
        $filename = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $filename );
        $filename = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $filename );
        $filename = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $filename );
        $filename = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),array('n', 'N', 'c', 'C'),
            $filename);
        $building->dependencias = str_replace("\n", "</li><li>", $building->dependencias);
        $output = 
        '<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
                <style>
                    body{
                        font:14px helvetica;
                        background-repeat: no-repeat; 
                        background-position: center;
                        background-attachment: fixed;       
                        webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }
                    h1{
                        font-weight: bold;
                        font-size: 20px;
                        right: 20px;
                        left: 9px;
                        color: #197EEA;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .contenido{
                        font-weight: bold;
                        font-size: 14px;
                        right: 20px;
                        left: 9px;
                        color: #282828;
                        text-shadow: #fff 0 1px 0;
                        padding: 1px 0 3px 8px;
                    }
                    .photoContainer {
                        overflow: auto;
                        height: auto;
                        width: 100%;
                        font-size: 20px;
                    }
                </style>
            </head>
            <body>
                <img width="100%" src="logo.png">
                <hr>
                <div class="photoContainer">
                    <img src="'.$filename.'.jpg" style="height: 250px;">
                </div>
                <h1>Descripci&oacute;n:</h1>
                <div class="contenido">
                    <!--Poner la descripcion del modulo aqui-->
                    '.$building->descripcion.'
                </div>
                <h1>Dependencias:</h1>
                <div class="contenido">
                    <ul>
                        <li>'.$building->dependencias.'</li>
                    </ul>
                </div>
            </body>
        </html>';
        $disk = Storage::disk(config('../storage/app'));
        if($filename != $lastNameFile){
            Storage::disk('public')->put($filename.'.jpg', Storage::disk('public')->get($lastNameFile.'.jpg'));
            Storage::disk('public')->delete($lastNameFile.'.jpg');
            Storage::disk('public')->delete($lastNameFile.'.html');
        }
        $filename = strtolower($filename);
        $filename .= ".html";
        if ($disk->exists(config('../storage/app/public') . '/' . $filename)) {
            $disk->delete(config('../storage/app/public') . '/' . $filename);
        }
        Storage::disk('public')->put($filename, $output);
        alert()->success('Edificio Editado correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }


    public function add() {
        $config = array();
        $config['center'] = '20.657395, -103.324954';
        $config['map_width'] = '100%' ;
        $config['map_height'] = 400;
        $config['zoom'] = 16;
        $config['onboundschanged'] = 
        
        'marker_0.setOptions({
            dragend: function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
            },
            position: new google.maps.LatLng(20.657395, -103.324954)
      

        });';

        \GMaps::initialize($config);

        $marker = array();
        $marker = array();
        $marker['position'] = '37.429, -122.1419';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
        \GMaps::add_marker($marker);

        $map = \GMaps::create_map();

        return view('buildings.add', compact('map','config','marker'));
    }

}
