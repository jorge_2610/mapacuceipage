<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use Artisan;
use Log;
use Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Filesystem\Filesystem;

class BackupController extends Controller
{

    public function test()
    {
        return view('backups.index');
    }

    public function index()
    {
        $disk = Storage::disk(config('../storage/app/public/backups'));
        $files = $disk->files(config('../storage/app/public/backups'));
        $backups = [];
        foreach ($files as $k => $f) {
            if (substr($f, -4) == '.sql' && $disk->exists($f)) {
                $backups[] = [
                    'file_path' => $f,
                    'file_name' => str_replace(config('../storage/app/public/backups') . '/', '', $f),
                    'file_size' => $this->human_filesize($disk->size($f)),
                    'last_modified' => $disk->lastModified($f),
                ];
            }
        }
        $backups = array_reverse($backups);
        return view("backups.index")->with(compact('backups'));
    }

    public function restore($file_name){
        $dbhost = '127.0.0.1';
        $dbname = 'arcucei';
        $dbuser = 'root';
        $dbpass = 'root';
        $dbdir = '../storage/app/public/backups/'. $file_name;
        $command = "C:\UniServerZ\core\mysql\bin\mysql --ignore-table=arcucei.users --password=root --user=root arcucei < " . $dbdir;
        system($command,$output);
        $file_name = str_replace('.sql', '.zip', $file_name);
        $Path = storage_path('app/public/'.$file_name);
        \Zipper::make($Path)->extractTo(storage_path('app'));
        alert()->success('Éxito.','La recuperación se ha realizado correctamente')->autoclose(3000);
        return redirect()->back();
    }

    public function download($file_name)
    {
        $file = config('../storage/app/public/backups') . '/' . $file_name;
        $disk = Storage::disk(config('../storage/app/public/backups'));
        if ($disk->exists($file)) {
            $fs = Storage::disk(config('../storage/app/public/backups'))->getDriver();
            $stream = $fs->readStream($file);
            return \Response::stream(function () use ($stream) {
                fpassthru($stream);
            }, 200, [
                "Content-Length" => $fs->getSize($file),
                "Content-disposition" => "attachment; filename=\"" . basename($file) . "\"",
            ]);
        } else {
            abort(404, "The backup file doesn't exist.");
        }
    }

    public function delete($file_name)
    {
        $disk = Storage::disk(config('../storage/app/public/backups'));
        if ($disk->exists(config('../storage/app/public/backups') . '/' . $file_name)) {
            $disk->delete(config('../storage/app/public/backups') . '/' . $file_name);
        } else {
            abort(404, "The backup file doesn't exist.");
        }

        $file_name = str_replace('.sql', '.zip', $file_name);
        $exists = storage_path('public')->exists($file_name);
        dd($exists);
        return redirect()->back();
    }

    function human_filesize($bytes,$decimals=2){
        if($bytes < 1024){
            return $bytes . ' B';
        }

        $factor = floor(log($bytes,1024));

        return sprintf("%.{$decimals}f ",$bytes / pow(1024,$factor)) . ['B','KB','MB','GB','TB','PB'][$factor];
    }

    public function respaldoPhp(){
        $file = new Filesystem;
        $date =  date("Y-m-d-H-i-s");
        $dbhost = '127.0.0.1';
        $dbname = 'arcucei';
        $dbuser = 'root';
        $dbpass = 'root';
        $backup_file = $dbname . '_' . $date . '.sql';
        $dbdir = '../storage/app/public/backups/'. $backup_file;
        $command = "C:\UniServerZ\core\mysql\bin\mysqldump --user=$dbuser --password=$dbpass --host=$dbhost --databases arcucei --ignore-table=arcucei.users > $dbdir";
        system($command,$output);
        $files = glob(storage_path('app/public'));
        \Zipper::make(storage_path('app/public/'.$dbname.'_'.$date.'.zip'))->add($files)->close();
        $test = 'showLoader(false);';
        return redirect()->back()->with('alert', 'El Backup se ha realizado correctamente');
        //return redirect()->back();
    }

}
