<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MapaCUCEI</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="css/marcadores.css">
        <link rel="stylesheet" href="css/post-ti.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <style type="text/css">
            .btn-info {
                background-color: #ff9e29 !important;
            }
        </style>
    </head>
    @include('sidebar')
        <div id="content">
            <div style="text-align: center;">
                <button type="button" id="sidebarCollapse" class="busqueda btn btn-info navbar-btn btn-sm"> 
                    <i class="glyphicon glyphicon-minus"></i> Ocultar Menú
                </button>
            </div>
            @forelse ($messages as $message)
                <div class="message-wrap" style="background-color: white">
                    <div class="msg-wrap">
                        <div class="media msg">
                            <div class="media-body">
                            <?php
                            $date = new DateTime($message->fecha);
                            ?>
                            <small style="color:black" class="pull-right time"><i class="glyphicon glyphicon-time"></i> {{$date->format('d/m/Y g:i:s A')}}
                            </small>
                                <p style="color:steelblue;font-weight:bold">{{$message->asunto}}</p>
                                <p>{{$message->contenido}}</p>
                            <form id="{{$message->id}}" class="pull-right" action="{{ route('messages.delete', $message->id) }}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('delete')}}
                                    <button style="display: none;" type="button" class="btn btn-xs btn-primary" data-toggle="modal"  data-target="#agent{{$message->id}}">Edit</button>
                                    <button type="button" class="btn btn-xs btn-danger btn-sm navbar-btn boton" onclick="confirmDelete({{$message->id}})"><i class="glyphicon glyphicon-trash"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            @endforelse
        </div>
        @routes
        <script>
                function confirmDelete(item_id) {
                    swal({
                        title: "¿Estás seguro?",
                        text: "Puedes recuperar este edifcio siempre y cuando hayas hecho un respaldo.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            console.log(item_id);
                            $('#'+item_id).submit();
                            swal("¡El Edificio se ha eliminado correctamente!", {
                                icon: "success",
                            });
                        } else {
                            swal("Operación Cancelada!");
                        }
                    });
                }
        </script>
</html>