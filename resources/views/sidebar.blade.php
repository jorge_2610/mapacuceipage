
    <body>
        <link rel="icon" href="icono.ico">

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3>Mapa CUCEI</h3>
                </div>

                <ul class="list-unstyled components">
                    <li><a>{{ Auth::user()->email }}</a></li><hr>
                    <li >
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"> <i class="glyphicon glyphicon-map-marker"></i> Marcadores</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li><a href="/edificios">Edificios</a></li>
                            <li><a href="/auditorios">Auditorios</a></li>
                            <li><a href="/servicios">Servicios</a></li>
                            <li><a href="/cafeterias">Cafeterías</a></li>
                            <li><a href="/papelerias">Papelerías</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/mensajes"> <i class="glyphicon glyphicon-envelope"></i> Mensajería</a>
                        <a href="/configuracion"> <i class="glyphicon glyphicon-cog"></i> Configuración</a>
                        <a href="/backups"> <i class="glyphicon glyphicon-save"></i> Respaldos</a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <li>
                        <a href="{{ route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="glyphicon glyphicon-log-out"></i> Logout
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content, #instruccion').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
   