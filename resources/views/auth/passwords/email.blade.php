@if(Auth::user())
    <script>window.location = "/edificios"</script>
@else
<html lang="es">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="icon" href="icono.ico">

        <title>Mapa CUCEI</title>

        <!-- Custom styles for this template -->
        <link href="/css/login.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <form class="form-signin" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <img class="mb-4" src="../icono.ico" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Reenvío de contraseña</h1>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="inputEmail" class="sr-only">Email address</label>
            <div class="col-md-12">
                <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-success">
                Enviar link para recuperar contraseña
            </button>
        </div>
        <div class="form-group text-center">
            <a type="button" href="/login" class="btn btn-primary">
                Volver al Login
            </a>
        </div>
    </form>
  </body>
</html>
@endif