<html>
    <head>
        <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MapaCUCEI</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
        <link rel="stylesheet"href="{{ asset('css/input-image.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery.fileupload.css')}}">
        <script type="text/javascript">var centreGot = false;</script>
        {!!$map['js']!!}
    </head>
    <body>
        @include('sidebar')
        <div id="content" class="text-center">
            <a href="/servicios">
                <button type="button" class="btn btn-responsive btn-outline-secondary mb-1 mt-1">
                    <i class="glyphicon glyphicon-chevron-left"></i> Regresar
                </button>
            </a>
            <form action="{{ url('editService') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row align-items-center">
                    <input type="text" name="id" value="{{$service->id}}" style="display:none;">
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">@</span>
                        <input required type="text" name="nombre" value="{{$service->nombre}}" pattern="[a-zA-ZáéíóúñÁÉÍÓÚÑ, 0-9]{2,100}" class="form-control" placeholder="Nombre del servicio (no caracteres especiales)" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Descripción</span>
                        <textarea required class="form-control" placeholder="Descripción" name="descripcion" rows="1">{{$service->descripcion}}</textarea>
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Servicios</span>
                        <textarea required class="form-control" name="servicios" placeholder="Servicios" rows="1">{{$service->servicios}}</textarea>
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Teléfono</span>
                        <input style="line-height:20px!important;" required type="tel" value="{{$service->telefono}}" class="form-control" placeholder="Hora Apertura" name="telefono" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Hora de Apertura</span>
                        <input style="line-height:20px!important;" min="07:00" max="21:00" required type="time" value="{{$service->hora_apertura}}" class="form-control" placeholder="Hora Apertura" name="hora_apertura" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group" style="margin-bottom:10px;">
                        <span class="input-group-addon" id="basic-addon1">Hora de Cierre</span>
                        <input style="line-height:20px!important;" min="07:00" max="21:00" name="hora_cierre" required type="time" value="{{$service->hora_cierre}}" class="form-control" placeholder="Hora Cierre" aria-describedby="basic-addon1">
                    </div>
                    <span>Latitud: </span><input style="margin:5px 0px 5px 0px;border-radius: 15px;" id="latitudes" readonly name="latitudes"  value="{{$service->latitud}}" /><br>
                    <span>Longitud: </span><input style="margin-bottom:5px;border-radius: 15px;" id="longitudes" readonly name="longitudes" value="{{$service->longitud}}" /><br>
                    {!!$map['html']!!}
                    <div class="text-center" style="margin-top: 20px;">
                        <button type="submit" class="btn btn-primary mb-2 text-center">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Actualizar
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        @include('sweet::alert')
        <script type="text/javascript">
            function updateDatabase(newLat, newLng)
            {
                document.getElementById("latitudes").value = newLat;
                document.getElementById("longitudes").value = newLng;
            }
        </script>
    </body>
</html>