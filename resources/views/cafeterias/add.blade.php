<html>
    <head>
        <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MapaCUCEI</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
        <link rel="stylesheet"href="{{ asset('css/input-image.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery.fileupload.css')}}">
        <script type="text/javascript">var centreGot = false;</script>
        {!!$map['js']!!}
    </head>
    <body>
        @include('sidebar')
        <div id="content" class="text-center">
            <a href="/cafeterias">
                <button type="button" class="btn btn-responsive btn-outline-secondary mb-1 mt-1">
                    <i class="glyphicon glyphicon-chevron-left"></i> Regresar
                </button>
            </a>
            <form action="{{ url('addCafeteria') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row align-items-center">
                    <input type="text" name="id" style="display:none;">
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">@</span>
                        <input required type="text" name="nombre" pattern="[a-zA-ZáéíóúñÁÉÍÓÚÑ, 0-9]{2,100}" class="form-control" placeholder="Nombre de la Cafetería (no caracteres especiales) " aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Descripción</span>
                        <textarea required name="descripcion" placeholder="Descripción" class="form-control" rows="1"></textarea>
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Menú</span>
                        <textarea required name="menu" placeholder="Menú" class="form-control" rows="2"></textarea>
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Hora de Apertura</span>
                        <input style="line-height:20px!important;" required type="time" class="form-control" name="hora_apertura" min="07:00" max="21:00" placeholder="Hora Apertura" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group" style="margin-bottom:10px;">
                        <span class="input-group-addon" id="basic-addon1">Hora de Cierre</span>
                        <input style="line-height:20px!important;" required type="time" class="form-control" name="hora_cierre" placeholder="Hora Cierre" min="07:00" max="21:00" aria-describedby="basic-addon1">
                    </div>
                    <div class="fileupload-buttonbar text-center">
                        <div class="col-xs-12">
                            <span class="fileinput-button input-group-text">
                                <i class="glyphicon glyphicon-camera"></i>
                                <span> Añadir Imagen a la Cafetería</span>
                                <input required accept=".jpg"  id="file-input" name="files[]" type="file"/>
                            </span>
                        </div>
                        <div class="container photoContainer text-center mb-2"id="preview"></div>
                    </div>
                    <div style="margin-top: 10px;">
                    <span>Latitud: </span>
                    <input style="margin:5px 0px 5px 0px;border-radius: 15px;" id="latitudes" value="20.657395" readonly name="latitudes"/><br>
                    <span>Longitud: </span><input style="margin-bottom:5px;border-radius: 15px;" id="longitudes" value="-103.324954" readonly name="longitudes" /><br>
                    {!!$map['html']!!}
                    <div class="text-center" style="margin-top: 20px;">
                        <button type="submit" class="btn btn-primary mb-2 text-center">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar Cafeteria
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        @include('sweet::alert')
        <script type="text/javascript">
            function previewImages() {
                document.getElementById('preview').innerHTML = "";
                var preview = document.querySelector('#preview');
                readAndPreview(this.files[0]);
                function readAndPreview(file) {
                    var reader = new FileReader();
                    reader.addEventListener("load", function() {
                    var image = new Image();
                    image.height = 200;
                    image.title  = file.name;
                    image.src    = this.result;
                    image.style['padding'] = '5px';
                    preview.appendChild(image);
                    }, false);
                    reader.readAsDataURL(file);
                }
            }
            document.querySelector('#file-input').addEventListener("change", previewImages, false);
            function updateDatabase(newLat, newLng)
            {
                document.getElementById("latitudes").value = newLat;
                document.getElementById("longitudes").value = newLng;
            }
        </script>
    </body>
</html>