@extends('layouts.app')

@section('content')
<link rel="icon" href="icono.ico">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default text-center">
                <br>
                <img class="mb-4" src="../../icono.ico" alt="" width="72" height="72">
                <h1 class="h3 mb-3 font-weight-normal">Mapa Cucei </h1>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    Ya se ha iniciado Sesión
                    <div style="margin-top: 10px;">
                        <a href="/" class="btn btn-outline-secondary">Ir a página de inicio</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
