<html>
    <head>
        <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MapaCUCEI</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
        <link rel="stylesheet"href="{{ asset('css/input-image.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery.fileupload.css')}}">
        <script type="text/javascript">var centreGot = false;</script>
        {!!$map['js']!!}
    </head>
    <body>
        @include('sidebar')
        <div id="content" class="text-center">
            <a href="/edificios">
                <button type="button" class="btn btn-responsive btn-outline-secondary mb-1 mt-1">
                    <i class="glyphicon glyphicon-chevron-left"></i> Regresar
                </button>
            </a>
            <form action="{{ url('addBuilding') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row align-items-center">
                    <div class="input-group mb-2">
                        <span class="input-group-addon" id="basic-addon1">@</span>
                        <input name="nombre" required type="text"  pattern="[a-zA-ZáéíóúñÁÉÍÓÚÑ, 0-9]{2,100}" class="form-control" placeholder="Nombre del Edificio (no caracteres especiales) " aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-2">
                        <span class="input-group-addon" id="basic-addon1">Descripción</span>
                        <textarea name="descripcion" placeholder="Descripción" required class="form-control" rows="1"></textarea>
                    </div>
                    <div class="input-group mb-2">
                        <span class="input-group-addon" id="basic-addon1">Dependencias</span>
                        <textarea name="dependencias" placeholder="Dependencias" required class="form-control" rows="3"></textarea>
                    </div>
                    <div class="fileupload-buttonbar text-center">
                        <div class="col-xs-12">
                            <span class="fileinput-button input-group-text">
                                <i class="glyphicon glyphicon-camera"></i>
                                <span> Añadir Imagen al Edificio</span>
                                <input required accept=".jpg"  id="file-input" name="files[]" type="file"/>
                            </span>
                        </div>
                        <div class="container photoContainer text-center mb-2"id="preview"></div>
                    </div>
                    <div style="margin-top: 10px;">
                    <span>Latitud: </span>
                    <input style="margin-bottom:5px;border-radius: 15px;" id="latitudes" value="20.657395" required readonly name="latitudes" /><br>
                    <span>Longitud: </span>
                    <input style="margin-bottom:5px;border-radius: 15px;" id="longitudes" value="-103.324954" readonly readonly name="longitudes" /><br>
                    </div>
                    {!!$map['html']!!}
                    <div class="text-center" style="margin-top: 20px;">
                        <button type="submit" class="btn btn-primary mb-2 text-center">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        @include('sweet::alert')
        <script type="text/javascript">
            function previewImages() {
                document.getElementById('preview').innerHTML = "";
                var preview = document.querySelector('#preview');
                readAndPreview(this.files[0]);
                function readAndPreview(file) {
                    var reader = new FileReader();
                    reader.addEventListener("load", function() {
                    var image = new Image();
                    image.height = 200;
                    image.title  = file.name;
                    image.src    = this.result;
                    image.style['padding'] = '5px';
                    preview.appendChild(image);
                    }, false);
                    reader.readAsDataURL(file);
                }
            }
            document.querySelector('#file-input').addEventListener("change", previewImages, false);
            function updateDatabase(newLat, newLng)
            {
                document.getElementById("latitudes").value = newLat;
                document.getElementById("longitudes").value = newLng;
            }
        </script>
    </body>
</html>