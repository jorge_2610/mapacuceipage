<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MapaCUCEI</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="css/marcadores.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <style type="text/css">
            .btn-info {
                background-color: #ff9e29 !important;
            }
        </style>
    </head>

    @include('sidebar')
    <body>
        <div id="content" class="text-center">
            <button type="button" id="sidebarCollapse" class="busqueda btn btn-info navbar-btn btn-sm"> <i class="glyphicon glyphicon-minus"></i> Ocultar Menú
            </button>
            <br>
            <a href="{{ route('buildings.add')}}" type="button" class="busqueda btn btn-success navbar-btn btn-sm"> <i class="glyphicon glyphicon-plus"></i> Agregar
            </a>
            <div style="background: white;" class="table-responsive">          
              <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Editar</th>
                        <th class="text-center">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($buildings as $building)
                    <tr>
                        <td style="vertical-align:middle">{{$building->nombre}}</td>
                        <td>
                            <a href="{{ route('buildings.edit', ['id' => $building-> id]) }}" type="button" id="sidebarCollapse" class="btn btn-xs btn-sm btn-info navbar-btn">
                            &#x270E; Editar</a>
                        </td>
                        <td>
                        <form id="{{$building->id}}" action="{{ route('buildings.delete', $building->id) }}" method="post">
                            {{csrf_field()}}
                            {{method_field('delete')}}
                            <button style="display: none;" type="button" class="btn btn-xs btn-primary" data-toggle="modal"  data-target="#agent{{$building->id}}">Edit</button>
                            <button type="button" class="btn btn-xs btn-danger btn-sm navbar-btn boton" onclick="confirmDelete({{$building->id}})">&#x2716; Eliminar</button>
                        </form>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
               </table>
            </div>
       </div>
    </body>
    @routes
    <script>
            function confirmDelete(item_id) {
                swal({
                    title: "¿Estás seguro?",
                    text: "Puedes recuperar este edifcio siempre y cuando hayas hecho un respaldo.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        console.log(item_id);
                        $('#'+item_id).submit();
                        swal("¡El Edificio se ha eliminado correctamente!", {
                            icon: "success",
                        });
                    } else {
                        swal("Operación Cancelada!");
                    }
                });
            }
    </script>
</html>