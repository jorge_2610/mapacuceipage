<html>
    <head>
        <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MapaCUCEI</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
        <link rel="stylesheet" href="{{ asset('css/marcadores.css') }}.">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet"href="{{ asset('css/input-image.css') }}">
        <script>
            function updateDatabase(newLat, newLng)
            {
                document.getElementById("latitudes").value = newLat;
                document.getElementById("longitudes").value = newLng;
            }
        </script>
        <script type="text/javascript">var centreGot = false;</script>{!!$map['js']!!}
    </head>
    <body>
        @include('sidebar')
        <div id="content" class="text-center">
            <a href="/edificios">
                <button type="button" class="btn btn-responsive btn-outline-secondary mb-1 mt-1">
                    <i class="glyphicon glyphicon-chevron-left"></i> Regresar
                </button>
            </a>
            <form action="{{ url('editBuilding') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row align-items-center">
                    <input name="id" value="{{$building -> id}}" style="display: none;">
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">@</span>
                        <input name="nombre" required type="text" value="{{$building->nombre}}" pattern="[a-zA-ZáéíóúñÁÉÍÓÚÑ, 0-9]{2,100}" class="form-control" placeholder="Nombre del Auditorio (no caracteres especiales) " aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Descripción</span>
                        <textarea name="descripcion" required class="form-control" rows="1">{{$building->descripcion}}</textarea>
                    </div>
                    <div class="input-group" style="margin-bottom:10px">
                        <span class="input-group-addon" id="basic-addon1">Dependencias</span>
                        <textarea name="dependencias" required class="form-control" rows="3">{{$building->dependencias}}</textarea>
                    </div>
                    <span>Latitud: </span>
                    <input style="margin-bottom:5px;border-radius: 15px;" id="latitudes" readonly name="latitudes"  value="{{$building->latitud}}" /><br>
                    <span>Longitud: </span>
                    <input style="margin-bottom:5px;border-radius: 15px;" id="longitudes" readonly name="longitudes" value="{{$building->longitud}}" /><br>
                    {!!$map['html']!!}
                    <div class="text-center" style="margin-top: 20px;">
                        <button type="submit" class="btn btn-primary mb-2 text-center">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Actualizar
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        @include('sweet::alert')
    </body>
</html>