<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MapaCUCEI</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="css/formconfig.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <style type="text/css">
            .btn-info {
                background-color: #ff9e29 !important;
            }
        </style>
    </head>
    
    @include('sidebar')
        <div id="content" class="text-center">
            <button type="button" id="sidebarCollapse" class="busqueda btn btn-info navbar-btn btn-sm"> 
                <i class="glyphicon glyphicon-minus"></i> Ocultar Menú
            </button>
            <div style="background-color: white; padding-bottom: 10px; margin-bottom: 10px;">
                <div class="form__top">
                    <h3>Actualizar Correo</h3>
                </div>      
                <form class="form__reg" method="POST" action="{{ route('changeEmail') }}">
                    {{ csrf_field() }}
                    <input class="input" type="email" name="email" placeholder="Actual Correo" required>
                    <input class="input" type="email" name="new-email" placeholder="Nuevo Correo" required>
                    <button type="submit" class="btn btn-primary mb-2 text-center btn-sm">
                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Actualizar Correo
                    </button>
                </form>
            </div>
            <div style="background-color: white; padding-bottom: 10px;">
                <div class="form__top">
                    <h3>Actualizar Contraseña</h3>
                </div>		
                <form class="form__reg" method="POST" action="{{ route('changePassword') }}">
                    {{ csrf_field() }}
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <input class="input" type="password" name="current-password" placeholder="Actual Contraseña" required>
                    <input class="input" type="password" name="new-password" placeholder="Nueva Contraseña" required>
                    <input class="input" type="password" name="new-password_confirmation" placeholder="Repetir Contraseña" required>
                    <button type="submit" class="btn btn-primary mb-2 text-center btn-sm">
                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Actualizar Contraseña
                    </button>
                </form>
            </div>
        </div>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        @include('sweet::alert')
</html>