<head>
    <link rel="icon" href="icono.ico">

    <title>MapaCUCEI</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="../../css/sidebar.css">
    <link rel="stylesheet" href="../../css/marcadores.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <style>
        .input-group-addon {
            padding: 6px 12px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1;
            color: white;
            text-align: center;
            background-color: steelblue;
            border: 1px solid steelblue;
        }
    </style>
    <style type="text/css">
        .preload {
            width: 100%;
            height: 100%;
            background: #333;
            opacity: 0.2;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1;
        }

        .logo {
            height: 70px;
            margin: 150px auto 50px auto;
            font-size: 50px;
            text-shadow : -1px 2px 2px #000;
            text-align: center;
            color: azure;
        }

        .loader-frame {
            width: 70px;
            height: 70px;
            margin: auto;
            position: relative;
            text-align: center;
            right: 55px;
        }

        .loader1, .loader2 {
            position: absolute;
            border: 5px solid transparent;
            border-radius: 50%;
        }

        .loader1 {
            width: 200px;
            height: 200px;
            border-top: 5px solid azure;
            border-bottom: 5px solid azure;
            animation : clockwisespin 1s linear infinite;
        }

        .loader2 {
            width: 190px;
            height: 190px;
            border-left: 5px solid #ff9e29;
            border-right: 5px solid #ff9e29;
            top: 5px; left: 5px;
            animation : anticlockwisespin 1s linear infinite;
        }
        @keyframes clockwisespin{
            from {transform: rotate(0deg);}
            to {transform: rotate(360deg);}
        }

        @keyframes anticlockwisespin{
            from {transform: rotate(0deg);}
            to {transform: rotate(-360deg);}
        }
    </style>
  </head>
<body>
    <div class="preload" id="loader">
        <div class="logo">
            Generando Respaldo 😘😍❤
        </div>
        <div class="loader-frame">
            <div class="loader1" id="loader1"></div>
            <div class="loader2" id="loader2"></div>
        </div>
    </div>
    @extends('sidebar')
    <div id="content" style="text-align: center;">
        @if (session('alert'))
            <div class="alert alert-success">
                {{ session('alert') }}
            </div>
        @endif
        <div class="text-center">
            <h3 style="color: #fc9414">Administrar Respaldos de la Base de Datos</h3>
        </div><br>
        <div class="text-center">
            <div class="text-center">
                <a id="create-new-backup-button" href="{{ url('create') }}" onclick="return showLoader(true);" class="btn btn-primary"
                   style="margin-bottom:2em;"><i
                        class="fa fa-plus"></i> Realizar Respaldo
                </a>
            </div>
            <div class="text-center">
                @if (count($backups))
    
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Archivo</th>
                            <th>Fecha</th>
                            <th class="text-center">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($backups as $backup)
                            <tr>
                                <td>{{ $backup['file_name'] }}</td>
                                <td>
                                    {{date("d/m/Y H:i:s A",$backup['last_modified'])}}
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-info"
                                       href="{{ url('backup/restore/'.$backup['file_name']) }}"><i
                                            class="fa fa-cloud-download"></i> Restaurar</a>
                                    <a class="btn btn-xs btn-primary"
                                       href="{{ url('backup/download/'.$backup['file_name']) }}"><i
                                            class="fa fa-cloud-download"></i> Descargar</a>
                                    <a class="btn btn-xs btn-danger" data-button-type="delete"
                                       href="{{ url('backup/delete/'.$backup['file_name']) }}"><i class="fa fa-trash-o"></i>
                                        Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="well">
                        <h4>There are no backups</h4>
                    </div>
                @endif
            </div>
        </div>
   </div>
   <script type="text/javascript">
       function showLoader(show) {
            if(show === true){
                document.getElementById("loader").style.display = "block";
            }
            else {
                document.getElementById("loader").style.display = "none";
            }
        }
        showLoader(false);
        function showAlert(){
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showConfirmButton:false,
              confirmButtonText: 'Yes, delete it!'
            })
        }
   </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @include('sweet::alert')
</body>
