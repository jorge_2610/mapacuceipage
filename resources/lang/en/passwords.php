<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener mínimo longitud de 6 caracteres.',
    'reset' => 'Tu contraseña se restableció correctamente',
    'sent' => 'Hemos enviado un link de recuperación de contraseña a tu correo',
    'token' => 'Este token es inválido o ya ha expirado',
    'user' => "No pudimos encontrar un usuario con esa dirección de correo electrónico.",

];
