-- MySQL dump 10.13  Distrib 5.6.35, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: arcucei
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `arcucei`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `arcucei` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `arcucei`;

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria`
--

LOCK TABLES `auditoria` WRITE;
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
INSERT INTO `auditoria` VALUES (1,'Auditorio Antonio Rodríguez','Auditorio localizado en el módulo E.',20.655551,-103.326642,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Auditorio Jorge Matute Remus','Auditorio localizado a un lado del módulo L.',20.656918,-103.325667,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Auditorio Enrique Díaz de León','Auditorio Ubicado en el módulo A.',20.654013,-103.325731,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Auditorio del Módulo Y','Auditorio ubicado en el módulo Y.',20.658168,-103.327672,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Auditorio Antonio Alatorre','Es el audtiorio que esta a un lado de la coordinacion de la division en electronica, esta escondido ya que pocas conferencias y/o eventos son alojados ahí.',20.657382077,-103.326425909,'2019-03-24 20:33:15','2019-03-24 20:33:15');
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buildings`
--

DROP TABLE IF EXISTS `buildings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buildings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `descripcion` text NOT NULL,
  `dependencias` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildings`
--

LOCK TABLES `buildings` WRITE;
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
INSERT INTO `buildings` VALUES (1,'Módulo A',20.654001,-103.32584,'En este módulo se encuentra la rectoría y las oficinas administrativas del CUCEI, además del auditorio Enrique Díaz de León','Rectoría\r\nProulex\r\nAuditorio Enrique Díaz de León\r\nSecretaría Académica\r\nSecretaría Administrativa\r\nContraloría\r\nCoordinación de Comisiones del Consejo de Centro\r\nCoordinación de Control Escolar\r\nCoordinación de Extensión\r\nCoordinación de Finanzas\r\nCoordinación de Personal\r\nCoordinación de Servicios Académicos\r\nCoordinación de Servicios Generales\r\nFinanzas Nóminas\r\nMaestría en Protección Ambiental\r\nUnidad de Adquisiciones y Suministros\r\nUnidad de Becas e Intercambios\r\nUnidad de Difusión\r\nUnidad de Enseñanza Incorporada\r\nUnidad de Multimedia Instrucciones\r\nUnidad de Personal Administrativo\r\nUnidad de Planeación\r\nUnidad de Presupuesto\r\nUnidad de Servicio Social\r\nRegistro de Títulos\r\nSala de Comisiones Especiales\r\nSala De Exámenes de Postgrado','2019-03-24 01:54:04','2019-03-24 07:54:04'),(2,'Módulo B',20.654015,-103.325049,'En este módulo se imparten clases para los alumnos pertenecientes a la Licenciatura en Cultura Física.','Área de Cultura Física.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Módulo C',20.654241,-103.325178,'En este módulo se imparten clases para los alumnos pertenecientes a la Licenciatura en Cultura Física.','Área de Cultura Física.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Módulo D',20.654522,-103.325478,' Es un módulo ubicado atrás de la biblioteca, sólo contiene laboratorios de ciencias químicas. ','Laboratorio de procesamiento de polímeros\r\nLaboratorio de procesos biotecnológicos\r\nLaboratorio de síntesis orgánica\r\nLaboratorio de geoquímica\r\nLaboratorio de electroquímica\r\nLaboratorio de bioinformática e ingeniería metabólica\r\nLaboratorio de morfología\r\nLaboratorio de microbiología de alimentos\r\nLaboratorio de química del estado sólido\r\n\r\n','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Módulo E',20.655561,-103.325988,'Módulo de dos pisos del área de ciencias químicas contiene 20 aulas y múltiples laboratorios. ','Laboratorio de química orgánica\r\nLaboratorio de análisis cuantitativo\r\nLaboratorio de química general\r\nLaboratorio de análisis cualitativo\r\nAuditorio Antonio Rodríguez\r\nLaboratorio de química farmacéutica\r\nLaboratorio análisis farmacéutico\r\nCubículos de maestros de química\r\nMuseo de mineralogía\r\nServicios médicos integrales\r\nJefatura del departamento de ingeniería química\r\nCoordinación de la licenciatura de ingeniería química\r\nLaboratorio de mineralogía\r\n','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Módulo F',20.655797,-103.325985,'Es un módulo del área de ciencias químicas con 10 aulas de clases y un laboratorio.','Laboratorio de espectroscopía.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Módulo G',20.655842,-103.326894,'Módulo dedicado a ingeniería química que cuenta con dos aulas.','Laboratorio de ingeniería química','0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Módulo H',20.656055,-103.326004,'Módulo del área de ciencias químicas que solo contiene laboratorios. ','Laboratorio de bacteriología docencia\r\nLaboratorio de análisis externos\r\nLaboratorio de microbiología sanitaria\r\nLaboratorio de caracterización de polímeros\r\nLaboratorio de biología molecular\r\nLaboratorio de química clínica\r\nLaboratorio de polímeros\r\nLaboratorio de servicio social\r\nLaboratorio de microbiología industrial\r\nComité de alumnos de la división de ciencias básicas\r\n','0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Módulo I',20.656056,-103.32563,'Es un módulo del área de ciencias químicas que sólo contiene 8 aulas de clases. ','Área de Ciencias Químicas','0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Módulo J',20.656219,-103.326035,'Módulo con múltiples laboratorios y oficinas del área de ciencias químicas.','Laboratorio de automatización y robótica\r\nLaboratorio de bacteriología\r\nLaboratorio de microbiología sanitaria\r\nLaboratorio de tecnología farmacéutica\r\nCoordinación de la maestría en sistemas de calidad\r\nUnidad de vinculación\r\nMaestría en ciencias químicas\r\n\r\n','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'Módulo K',20.656365,-103.326121,'Es un pequeño módulo ubicado en el área de ciencias químicas.','Aula de dibujo 1.\r\nUnidad interna de protección civil.\r\n\r\n','0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'Módulo L',20.656759,-103.32521,'Este módulo pertenece a la división de ciencias básicas.','División de ciencias básicas.\r\nLaboratorio de mecánica eléctrica.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'Módulo M',20.656659,-103.32617,'En este modulo se encuentran los Laboratorios de Investigación.','Laboratorio de prototipado\r\nLaboratorio de Programación para Móviles\r\nLaboratorio de Sistemas de Comunicación Inalámbrica','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'Módulo N',20.656963,-103.326216,'Este módulo pertenece a la división de electrónica y computación.','Radio Cucei\r\nLaboratorio de Redes\r\nLaboratorio de Electrónica\r\nLaboratorio de Biomédica\r\n\r\n','0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'Módulo O',20.657294,-103.326254,'Este módulo era la antigua biblioteca del CUCEI, actualmente en él se encuentran varios cubículos de maestros y coordinaciones de la división de electrónica y computación y división de ingenierías.','Auditorio Antonio Alatorre\r\nCoordinación de licenciatura en ingeniería en computación\r\nCoordinación de licenciatura en ingeniería informática <3\r\nCoordinación de licenciatura en ingeniería biomédica\r\nCoordinación de licenciatura en ingeniería en comunicaciones y electrónica\r\nCoordinación de ingeniería mecánica eléctrica\r\nCoordinación de licenciatura en ingeniería industrial\r\nCoordinación de Licenciatura en ingeniería topográfica\r\nCubículos de maestros\r\nDepartamento de ingeniería industrial\r\nDepartamento de ciencias computacionales\r\nDepartamento de ingeniería civil y topográfica\r\nDepartamento de ingeniería mecánica eléctrica\r\nDivisión de electrónica y computación\r\nDivisión de ingenierías\r\nSala de maestros de electrónica y computación','0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,'Módulo P',20.657339,-103.325412,'Este módulo pertenece al departamento de ingeniería civil, tiene 23 aulas. ','Laboratorio de topografía','0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,'Módulo Q',20.6576,-103.32516,'Este módulo pertenece al departamento de ingeniería industrial tiene 25 aulas y 3 laboratorios. ','Laboratorio de Mecánica\r\nLaboratorio de Introducción a la física\r\nLaboratorio de óptica','0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,'Módulo R',20.65767,-103.32568,'Este módulo pertenece a los departamentos de ciencias computacionales y matemáticas, tiene 8 aulas y un laboratorio. ','Laboratorio de Redes.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,'Módulo S',20.657657,-103.326314,'El módulo S ubicado entre los módulos O y V cuenta con laboratorios de la Maestría en Ciencias en Ingeniería Electrónica y Computación, laboratorio de robótica, Site principal y oficinas.','Maestría en Ciencias en Ingeniería Electrónica y Computación\r\nLaboratorio de robótica\r\nOficinas de investigadores\r\nOficionas del STAUdeG\r\n\r\n','0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,'Módulo T',20.657885,-103.325526,'Es un módulo del área de ciencias matemáticas. ','Área de Ciencias Matemáticas.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'Módulo U',20.658101,-103.325486,'Módulo dedicado a matemáticas e ingeniería civil, cuenta con 14 aulas. ','Laboratorio de Concreto.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'Módulo V',20.658307,-103.326344,'Dedicado al departamento de matemáticas y física contiene las aulas de clase de estos departamentos, cuenta con 19 aulas.','Coordinación de la Lic. En física.\r\nSala audiovisuales.\r\nCoordinación de la licenciatura en matemáticas.\r\nAula de posgrado en ciencias de la hidrometeorología.\r\nCubículos de matemáticas.\r\nSala de lectura matemáticas.\r\nDepartamento de matemáticas.\r\nMaestría en ciencias en química.\r\n','0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,'Módulo V2',20.658104,-103.326249,'Pendiente de añadir.','Preguntar a Patty <3.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,'Módulo W',20.658078,-103.326925,'Este módulo, ubicado entre el CUCEI y la Prepa 12, está dedicado a computación y robótica, cuenta con 5 aulas. ','Comité de Matemáticas.\r\nComité de Física.\r\nComité de Computación.\r\nComité de Informática.\r\nOficina 1.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,'Módulo X',20.658253,-103.326967,'Dedicado a las Ciencias Computacionales, cuenta con 22 aulas.','1 Laboratorio.\r\nOficinas del Servicio Social.\r\nCubículos.\r\nOficina 1.','2019-03-24 04:19:53','2019-03-24 08:25:41'),(26,'Módulo Y',20.657869,-103.327549,'Es un módulo para exposiciones y laboratorios. Coloquialmente conocido como \"Chedraui\"','1 Auditorio.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,'Módulo Z',20.658414,-103.327674,'Módulo dedicado a las maestrías en física. ','Laboratorio de investigaciones.\r\nLaboratorio de caracterización de materiales.\r\nOficina 1.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,'Módulo Beta',20.656235,-103.325223,'Coming soon.','Coming soon.','0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,'Módulo Alfa',20.656467,-103.325228,'coming soon.','coming soon.','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cafeterias`
--

DROP TABLE IF EXISTS `cafeterias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cafeterias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `menu` text DEFAULT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `hora_apertura` time NOT NULL,
  `hora_cierre` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cafeterias`
--

LOCK TABLES `cafeterias` WRITE;
/*!40000 ALTER TABLE `cafeterias` DISABLE KEYS */;
INSERT INTO `cafeterias` VALUES (1,'El Globo','El Globo es un establecimiento de comida variada, muy popular entre los alumnos de CUCEI debido a su relación calidad-precios. Es un lugar ideal para comer ya que la higiene de los empleados de este lugar es muy buena.','*Tacos\r\n*Hamburguesa\r\n*Ensaladas\r\n*Molletes\r\n*Lonches\r\n*Quesadillas\r\n*Gringas\r\n*Hot Dogs\r\n*Bagettes\r\n*Postres\r\n*Etc.',20.655991,-103.325147,'09:00:00','19:00:00','2019-04-06 23:11:23','2019-04-06 23:11:23'),(2,'Locales del Módulo P',' En esta zona podemos encontrar establecimientos de comida variada. Estos locales se encuentran ubicados a un costado del Modulo P.\n','*Tortas\n*Hamburguesas\n*Sandwiches\n*Sushi\n*Pizza\n*Tacos\n*Agua fresca\n*Sodas\n*Postres\n*Comida chatarra \n*Entre otras',20.657394,-103.324788,'10:00:00','15:00:00','2019-04-06 19:05:01','0000-00-00 00:00:00'),(3,'Locales del Módulo X','En esta zona de la universidad podemos encontrar varios locales de comida, entre ellos destaca el local del Brother donde podemos encontrar un menu extenso e incluso especial hecho para los alumnos del CUCEI.','*Comida Casera\n*Ensaladas\n*Comida Japonesa\n*Hot Dogs\n*Hamburguesas\n*Sandwiches\n*Baggetes\n*Tacos\n*Gringas\n*Etc.',20.658805,-103.326617,'10:00:00','19:00:00','2019-04-06 19:05:33','0000-00-00 00:00:00'),(4,'Bebidas y Refrigerios','En esta zona de la universidad podemos encontrar 3 locales variados distribuidos por todo el campus.','*Bebidas Calientes (Cafe, chocolate, Te chai, Expresos, etc.)\n*Bebidas Frias (Malteadas, Frappe, etc.)\n*Agua de Fruta\n*Verdura (Chayote, papa, elote, Brocoli, Betabel, etc.)\n*Tostielotes\n*Pan de Elote\n*Papas Doradas\n*Snacks\n*Comida chatarra en general\n*Etc.',20.655857636,-103.325105753,'07:00:00','20:00:00','2019-04-06 19:06:25','0000-00-00 00:00:00'),(5,'Local del Modulo U','Este local se encuentra en el Modulo U, situado en el centro junto a las escaleras para subir al segundo piso. Este local como tal no es una cafeteria, sin embargo, es una local muy util cuando se necesita comprar algo de comida chatarra.','*Gran variedad de dulces\n*Chocolates\n*Frituras\n*Bolis',20.658060791,-103.325348873,'09:00:00','15:00:00','2019-04-06 19:07:55','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `cafeterias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asunto` varchar(100) NOT NULL,
  `contenido` text NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'Bache por el X','En el camino que te lleva al X hay un bache.','0000-00-00 00:00:00'),(2,'Obra por el Q.','Camino al Q empezaron una obra por las tuberías que rompieron.','0000-00-00 00:00:00'),(3,'Hola','Esto es una prueba','0000-00-00 00:00:00'),(4,'hola','android.support.v7.widget.AppCompatEditText{fb1349a VFED..CL. .F...... 20,161-660,461 ','0000-00-00 00:00:00'),(5,'Otra prueba','Esto es una prueba para enviar mensajes largos y mensajes de texto y mensajes de texto y mensajes de texto y mensajes de texto y mensajes de texto y mensajes de texto y mensajes de texto y mensajes de texto y mensajes de texto y mensajes de texto y mensajes de texto. ','0000-00-00 00:00:00'),(6,'soy juan ','es una prueba v2 ','0000-00-00 00:00:00'),(7,'el coral blanco','el coral blanco y el ambiente que venimos manejando ','0000-00-00 00:00:00'),(8,'hormigas quimera ','hay hormigas quimera cerca del v','0000-00-00 00:00:00'),(9,'Holahola','Prueba ','0000-00-00 00:00:00'),(10,'Jajaja ','Otra prueba ','0000-00-00 00:00:00'),(11,'Hallo','Más pruebas por fa ya funciona bien ','0000-00-00 00:00:00'),(12,'Kskdkdls','Jajaja ya somos is one 1? I can see in this one 1? that you are looking to get into your life with the same as a special celebration ???? you are the one 1? and a woman ???? the same time ? in your popular life in a non romantic life in a few years or more and I look at your life in life that is not a true meaning for me ','0000-00-00 00:00:00'),(13,'prueva v3','hay una invasión de ciervos zombies por el H','0000-00-00 00:00:00'),(14,'Ya ','Jajaja ','0000-00-00 00:00:00'),(15,'Me perdi','Aiudaaaaaas','0000-00-00 00:00:00'),(16,'Kekdjgfjdlwi','Keidenekkdifkdnfkfj','0000-00-00 00:00:00'),(17,'Hooooola','Soy yo','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `servicios` text NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `telefono` varchar(35) NOT NULL,
  `hora_apertura` time NOT NULL,
  `hora_cierre` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Centro de Aprendizaje Global','El Centro de Aprendizaje Global (CAG) es un espacio en donde los alumnos interesados en aprender idiomas pueden encontrar una gran variedad de materiales y equipo multimedia que les facilitan el autoaprendizaje.\nSe encuentra ubicado en la planta alta del CID y cuenta con dos laboratorios de cómputo, además de áreas de lectura, video y audio.','-Los recursos didácticos con que cuenta, están diseñados especialmente para que mediante el auto aprendizaje, los estudiantes puedan comunicarse en una lengua extranjera, así como corregir y evaluar su propio progreso de una manera rápida y óptima.\n-Podrán encontrar materiales para el aprendizaje de francés, italiano, japonés, portugués, inglés, alemán, español, y ruso (para extranjeros); todos ellos en diferentes formatos, privilegiando el formato digital.',20.654639801,-103.325440486,'(33)13785900 Ext. 27484 y 27485','08:00:00','20:00:00','2019-03-31 01:53:01','0000-00-00 00:00:00'),(2,'Entrenamiento Deportivo','EL CUCEI fomenta la organización de equipos y torneos de carácter deportivo. En el CUCEI puedes participar en equipos de Futbol Femenil y Varonil, futbol bardas, voleibol, básquetbol, ajedrez, atletismo, béisbol, gimnasia aeróbica, halterofilia, judo, karate, handball, tae kwon do, tenis, entre otros.','Gracias a la ubicación privilegiada del CUCEI, se puede tener acceso a las instalaciones deportivas que se encuentran junto al centro universitario. Dichas instalaciones no pertenecen propiamente al centro universitario; pero por su carácter universitario, todos los integrantes del CUCEI pueden utilizarlas cubriendo los requisitos correspondientes.',20.656694185,-103.323642544,'(33)13785900 ext. 27425','09:00:00','20:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Internet y Telecomunicaciones','Sabemos de la importancia de contar y aprovechar los servicios que las nuevas tecnologías de la información y la comunicación (TIC) nos ofrecen, de ahí que el CUCEI realiza un esfuerzo importante para fortalecer una red de servicios relacionados con esta temática.\n\nUno de los más importantes es el internet inalámbrico, al cual puede conectarse en la gran mayoría de los espacios del CUCEI. Por la distribución de los edificios e instalaciones, así como la distribución de los usuarios, algunas zonas tienen una mejor recepción, como puede observarse en la imagen anexa. Para poder acceder a ella acude a la Coordinación de Tecnologías para el Aprendizaje (CTA) con tu computadora, teléfono o tableta, para registrar tu dispositivo.','La CTA cuenta con un centro de impresión, en el que con un bajo costo de recuperación, podrás imprimir en blanco negro y color; grabar CD y escanear. Además se realiza la detección y limpieza de virus informáticos. Dicho centro está ubicado en el módulo Gama.',20.656347083,-103.324942689,'(33) 13785900 ext. 27412','08:00:00','20:00:00','2019-03-31 02:04:51','0000-00-00 00:00:00'),(4,'Médicos y Clínicos','El Centro Universitario de Ciencias Exactas e Ingenierías ofrece a su comunidad servicios de salud integral dentro de sus instalaciones, ubicadas en el módulo “L”, dentro del sub-almacén.','Contamos con servicios de medicina general, psicología y nutrición con un horario de atención de 8:00 a 20:00 horas.',20.656684847,-103.325362455,'(33) 1378 5900 ext. 27603','08:00:00','20:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Servicios Bibliotecarios','En toda actividad educativa el acceso a las fuentes de información confiables y en particular a los libros y revistas, ya sea impresos o digitales, se convierte en una herramienta esencial para el éxito de la acción educativa. Por ello el CUCEI a través del Centro Integral de Documentación (CID), también conocido como biblioteca ha desarrollado un espacio, reconocido por su calidad y condiciones en donde tendrás acceso a libros, revistas e información electrónica como apoyo a tu formación.','Además de libros impresos, cuenta con un área especializada en revistas científicas, tesis, el denominado fondo histórico y número muy importante de libros en formato digital. Sus servicios también pueden ser obtenidos a través de internet, mediante el cual puedes consultar la Biblioteca Digital, el catálogo en línea (ALEPH) y bases de datos especializadas. Además, el CID brinda servicios como préstamo externo, préstamo de equipos de cómputo, cubículos de estudio, servicio de copiado e internet inalámbrico.',20.654824867,-103.32565595,'(33) 13785900 ext. 27489','08:00:00','20:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Talleres Culturales','Para fomentar la formación integral de los estudiantes, el CUCEI a través del Módulo de Actividades Culturales (MAC) dependiente de la coordinación de extensión, así como de las Divisiones y coordinaciones de carrera, organiza diferentes actividades relacionadas con el arte y el desarrollo de habilidades artísticas y la promoción de la cultura en lo general.','Dependiendo de la demanda los talleres que se ofrecen son: Baile: afroantillano, banda, danza árabe, danzas polinesias, jazz, regional y rock de los 60’s. Teatro, yoga, canto, guitarra clásica, Tuna de CUCEI femenil y varonil;  y violín. Ajedrez, capoeira, dibujo y pintura. Juegos de mesa alternativos. Talleres sobre comunicación y liderazgo, lectura veloz, oratoria y declamación.',20.654042465,-103.325820689,'(33)1378 5900, ext. 27424','09:00:00','15:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Trámites escolares','Para el Centro Universitario de Ciencias Exactas e Ingenierías, es sumamente importante mantener informados a los aspirantes, estudiantes y egresados acerca de los procesos administrativos relacionados con su trayectoria escolar.\nAdemás de los coordinadores de carrera, una de las dependencias que mayor responsabilidad tiene es la Coordinación de Control Escolar; la cual ha certificado los servicios que ofrece a través de la norma ISO:9001.','-Acreditación y revalidación de materias\n-Baja de materias\n-Baja voluntaria (devolución de documentos para alumnos admitidoss\n-Certificación de planes de estudio\n-Condonación\n-Constancias y certificados de estudio\n-Copias certificadas de documentos\n-Corrección de calificación\n-Devolución de documentos a no admitidos\n-Duplicado de documentos (credencial, hologramas)\n-Estados de cuenta\n-Examen de acreditación por competencia\n-Kardex legalizado\n-Pre-registro de materias\n-Seguro facultativo\n-Si deseas ausentarte un semestre necesitas\n-Solicitud de oportunidad por incurrir en el Art. 33\n-Solicitudes de licencia\n-Tramites de aclaraciones\n-Trámite de condonación\n-Trámites para la obtención del título',20.654075595,-103.325743576,'(33)13785900 ext. 27440','09:00:00','15:00:00','2019-03-31 02:44:37','0000-00-00 00:00:00'),(8,'Becas','El CUCEI impulsa y gestiona numerosas oportunidades de becas y apoyos para mejorar el desempeño de los estudiantes, egresados, académicos y trabajadores. Para ello promueve convenios, difunde las oportunidades y convocatorias de becas y auxilia en la gestión de las mismas.\n\nLas principales fuentes de financiamiento de las becas son con frecuencia instituciones externas, que a través de convocatorias establecen requisitos y periodos a cumplir para el otorgamiento de las mismas. Los requisitos dependerán del objetivo de la beca, pues algunas pueden ser para apoyo en la manutención durante los estudios, otras pueden ser para la realización de una estancia o intercambio, o para participar en un proyecto. ','Entre los principales programas de becas se encuentran, entre otras, los siguientes:\n-Becas Secretaría de Educación Pública, para manutención, servicio social, de excelencia académica, para vinculación, entre otras\n-Programa de Movilidad Estudiantil, para la realización de intercambio\n-Becas de la Secretaría de Relaciones Exteriores\n-Becas del CONACYT, para estudios de posgrado y estancias\n-Becas y apoyos para idiomas\n-Programa de apoyos a estudiantes sobresalientes\n-Apoyos para el estudio de idioma inglés.\n-Apoyos del CUCEI a través de Secretaría Académica.\n-Convocatorias de la Coordinación General de Cooperación e Internacionalización.\n-La Unidad de Becas e Intercambio se encuentra ubicada en la planta alta del CID.',20.65498013,-103.325333198,'13785900 ext. 27419 y 27420','08:00:00','20:00:00','2019-03-31 02:50:42','0000-00-00 00:00:00'),(9,'Bolsa de trabajo','La Bolsa de Trabajo del CUCEI, actúa como enlace entre alumnos, egresados y empresas, relaciona a egresados y alumnos con probables fuentes de trabajo y promueve sus perfiles profesionales.\r\n\r\nA través de la Bolsa de Trabajo, se constituye una acción estratégica para vincular la oferta educativa del CUCEI con el mercado de trabajo. Mediante este programa los empleadores hacen del conocimiento de la comunidad universitaria, diferentes ofertas de empleo para la incorporación temprana de los egresados al mercado laboral. ','El programa permite obtener información sobre demandas específicas tales como habilidades, conocimientos y actitudes que deben ser promovidas entre los estudiantes e incorporadas en los programas educativos como parte de sus estrategias de aprendizaje. \r\n\r\nEl servicio de Bolsa de Trabajo se complementa con la organización de ferias del empleo, las cuales se realizan periódicamente en las instalaciones del centro universitario. En dichas ferias, egresados, alumnos y público en general, tiene la oportunidad de entrar en contacto directo con las fuentes de empleo y satisfacer necesidades.',20.654866687,-103.325290283,'(33) 13785900 ext. 27574, 27575','09:00:00','17:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stationeries`
--

DROP TABLE IF EXISTS `stationeries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stationeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `hora_apertura` time NOT NULL,
  `hora_cierre` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stationeries`
--

LOCK TABLES `stationeries` WRITE;
/*!40000 ALTER TABLE `stationeries` DISABLE KEYS */;
INSERT INTO `stationeries` VALUES (1,'Papelería del Globo','La papeleria del Globo se encuentra ubicado a un costado de la Cafeteria El Globo, esta papeleria vende material util para el alumnado entre ellas podemos destacar  el material mas basico como cuadernos, lapiceras, lapices, sacaputas, etc. Ademas de contar con sistema de impresion a color y en blanco y negro.',20.655833,-103.325169,'09:45:00','20:00:00','2019-03-23 21:19:53','2019-03-24 03:19:53'),(2,'Papelería del N','La papeleria del Modulo N, se encuentra a un costado del Modulo N. Esta papeleria vende material util a sus alumnos, ademas de vender objetos como mochilas, juegos geometricos, estuches, cuadernos, etc.\nEsta papeleria destaca por brindar multiples servicios en los cuales encontramos:\n*Enmicados\n*Engargolados de Planos\n*Lavado de Estilografos\n*Servicios de Fax\n*Tiempo aire para tu celular\n*Mantenimiento, Reparacion y Configuracion de tu equipo de computo.\n*Forrado de Libros y Cuadernos\n*Servicio de Guillotina\n*Etc.',20.657039055,-103.326111207,'08:00:00','18:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Papelería del U','La papeleria del Modulo U se encuentra ubicado a la parte del frente del Modulo U, junto a las escaleras para subir al segundo piso, en esta pequeña papeleria se vende material util para el alumnado entre ellas podemos destacar el material mas basico como cuadernos, lapiceras, lapices, sacaputas, carpetas de diferentes colores, Ademas de contar con sistema de impresion a color y en blanco y negro. Algo muy iumportante es que esta papeleria no cuenta con conexion a internet por lo cual si es necesario imprimir algo, este debe ser llevado en una memoria USB. ',20.65806833,-103.325591362,'08:00:00','20:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Papelería del Globox','Descripción de la nueva Papelería',20.657395,-103.324954,'09:00:00','18:00:00','2019-04-06 23:17:22','2019-04-06 23:17:22');
/*!40000 ALTER TABLE `stationeries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'AR.Cucei2019@gmail.com','$2y$10$sCZFkBbypjCZcwRQsNa91eyfBsY.NC93j0XeldcHn4jsRnCSOrAcC','4XPnxkaf1ILkoa6XbPnpFD7IsJFxZsojz52W5R5tfmpl72u0wbws0NnmC21i','2019-02-27 05:08:41','2019-04-07 03:43:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-09 21:00:58
