function previewImages() {
    document.getElementById('preview').innerHTML = "";
    var preview = document.querySelector('#preview');
    readAndPreview(this.files[0]);
    function readAndPreview(file) {
        var reader = new FileReader();
        reader.addEventListener("load", function() {
        var image = new Image();
        image.height = 200;
        image.title  = file.name;
        image.src    = this.result;
        image.style['padding'] = '5px';
        preview.appendChild(image);
        }, false);
        reader.readAsDataURL(file);
    }
}
document.querySelector('#file-input').addEventListener("change", previewImages, false);